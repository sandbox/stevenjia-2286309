<?php

/**
 * @file
 * Callbacks for theme, menu.
 */

function theme_ckplayer_formatter_player(array $variables) {
  $item = $variables['item'];

  // 如果没有上传文件，返回空
  if (empty($item['fid'])) {
    return '';
  }

  if (!in_array($item['filemime'], array('video/x-flv', 'video/mp4'))) {
    return '<p>请上传flv或mp4格式的视频文件</p>';
  }

  $container_id = 'ckplayer-' . $item['fid'];

  $path = file_create_url($item['uri']);

  return ckplayer_add($container_id, $path);
}
